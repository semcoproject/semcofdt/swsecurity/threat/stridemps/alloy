/**
* STRIDE Asynchronous Module
*
* Define STRIDE properties for MPS communication in a Component Base Architecture
* using Message Passing Asynchronous based communication model
*/
module stride

open Architecture/cbsemetamodel
open Architecture/messaging
open Architecture/connectorMPS
open Architecture/constraint
open Architecture/property
open util/ordering[messaging/Tick] as tick


/** Threats */
/** Spoofing
* Sender of the message is always the originator of the message
*/
// Verification that nobody can pretend to send a message as somebody else.
sig SenderSpoofing extends ConnectorProperty  {}{
	not NotBeSpoofed[comp1,comp2,payl]
} 
pred NotBeSpoofed[c1,c2:Component, d:Payload] {
		all t:Tick | let m = {m:MsgPassing | m.payload = d }|
			E_get_src[c2,m,c1,t] implies sent_by[m,c1,t]
}
assert MPSCanBeSpoofed  {
    all c1,c2:Component, d:Payload | no SenderSpoofing.holds[c1,c2,d]
}

check MPSCanBeSpoofed for 4 but 5 Tick // CounterExample must be found

// Security Req: A spoofProof ConnectorMPS offers that all messages transmited by it have authentic callers
pred ConnectorMPS.spoofProof
{
	all mp:MsgPassing, t: Tick |
		mp in this.buffer.t implies
			mp.sender = mp.origin_sender // The caller is authentic
}

assert MPSCanNotBeSpoofed  {
	(all c:ConnectorMPS | c.spoofProof) implies ( all c1,c2:Component, d:Payload | no SenderSpoofing.holds[c1,c2,d])
}

check MPSCanNotBeSpoofed for 4 but 5 Tick // No CounterExample must be found


/** Tampering
* Refers to the unauthorized modification of data.
*/
sig PayloadTampering extends ConnectorProperty  {}{
	not NotBeAltered[comp1,comp2,payl]
} 

// Verification that nobody can alter a message during a transmittion.
pred NotBeAltered[c1,c2:Component, d:Payload] {
	all p:Payload, t:Tick |  let m = {m:MsgPassing | m.payload = d}{
			E_get_pld[c2,m,p,t] and sent_by[m,c1,t] implies sent_with[m,p,t]
	}
}

assert MPSCanBeAltered  {
    all c1,c2:Component, d:Payload | no PayloadTampering.holds[c1,c2,d]
}

check MPSCanBeAltered for 4 but 8 Tick // CounterExample must be found

// Security Req : A  tamperProof ConnectorMPS offers that all messages transmited by it can not be altered.
// Utility function that return a set of equivalent
// A  tamperProof ConnectorMPS offers that all messages transmited by it can not be altered.

pred  ConnectorMPS.tamperProof {
	all t,t':Tick, mp,mp':MsgPassing |
		mp in this.buffer.t
		implies (
				mp' not in this.buffer.t'
				and mp'.sender = mp.sender
				and mp'.receiver = mp.receiver
				and mp'.sent = mp.sent
		)
}
 
assert MPSCanNotBeAltered  {
    (all c:ConnectorMPS | c.tamperProof) implies ( all c1,c2:Component, d:Payload |  no PayloadTampering.holds[c1,c2,d] )
}

check MPSCanNotBeAltered for 4 but 8 Tick // No CounterExample must be found



/** Repudiation
*  Refers to a component claiming to have not performed an action that was performed.
*/
// Verification that no message can be received without being logged

sig SendReceiveRepudiation extends ComponentProperty  {}{
	not NotBeRepudiated[comp,payl]
} 

pred NotBeRepudiated[c:Component, d:Payload]{
	 all t1:Tick - tick/last, t2:Tick - tick/last | let m = {m:MsgPassing | m.payload = d }{
           ( inject[c,m, t1] implies all t1':t1.nexts | T_inject[c,m,t1']  ) 
			or
			( intercept[c,m, t2] implies all t2':t2.nexts | T_intercept[c,m,t2'] )
    }
}

assert MPSCanBeRepudiated  {
    all c:Component, d:Payload  | no SendReceiveRepudiation.holds[c,d]
}

check MPSCanBeRepudiated for 4 but 5 Tick // CounterExample must be found

// Security Req : A  repudiationProof Component offers that all messages injected/intercepted are logs indefinitely.
pred Component.repudiationProof {
    all m:MsgPassing, t:Tick | inject[this, m, t] implies Log.addEntry[m, t]
	all m:MsgPassing, t:Tick | intercept[this,m, t] implies Log.addEntry[m, t]
}

assert MPSCanNotBeRepudiated  {
    (all c:Component | c.repudiationProof) implies (all c:Component, d:Payload |  no SendReceiveRepudiation.holds[c,d])
}

check MPSCanNotBeRepudiated for 4 but 5 Tick // No CounterExample must be found


/** Information disclosure
* Refers to the unauthorized exposure of information to a component for which it is not intended.
*/
// Verification  that all messages are delivered only to the intended receivers
sig PayloadDisclosure extends ConnectorProperty  {}{
	not NotBeIntercepted[comp1,comp2,payl]
}
pred NotBeIntercepted[c1,c2:Component, d:Payload] {
	all c3:Component-c1-c2, t1:Tick, t2:t1.prevs | let m = {m:MsgPassing | m.payload = d } {
		inject[c1,m,t2] and m.receiver = c2 implies not intercept[c3,m,t1]
	}
}

assert MPSCanBeIntercepted  {
    all c1,c2:Component, d:Payload | no PayloadDisclosure.holds[c1,c2,d]
}

check MPSCanBeIntercepted for 4 but 5 Tick // CounterExample must be found

// Securitry req : A  informationDisclosureProof Connector offers that all messages transmited
// can not be intercepted by not intended Component.
pred  ConnectorMPS.informationDisclosureProof {
	all mp:MsgPassing, t:Tick |
	(mp in this.buffer.t) =>
			this.connects.t = mp.origin_sender.uses + mp.receiver.uses
}

assert MPSCanNotBeIntercepted  {
    (all c:ConnectorMPS | c.informationDisclosureProof) implies (all c1,c2:Component, d:Payload |  no PayloadDisclosure.holds[c1,c2,d])
}
check MPSCanNotBeIntercepted for 4 but 5 Tick // No CounterExample must be found


/** Denial of Service
* Refers to the unauthorized witholding of a service to system components
*/
// Verification that all messages are delivered to receivers before the payload of the message loses is freshness.
sig PayloadStaleness extends ConnectorProperty  {
}{
	not NotBeReceivedStale[comp1,comp2,payl]
}

pred NotBeReceivedStale[c1,c2:Component, d:Payload] {
    all i:Interval, t1:Tick, t2:t1.nexts | let m = {m:MsgPassing | m.payload = d }{
		inject[c1,m,t1] and m.receiver = c2 and d.freshness = i implies E_intercept[c2,m,t2] and  I_in[t2, d.freshness]
	}
}

assert MPSCanStale  {
     all c1,c2:Component, d:Payload | no PayloadStaleness.holds[c1,c2,d]
}

check MPSCanStale for 3 but 5 Tick // CounterExample must be found

// Security req : A  payloadStalenessProof Connector offers that all messages transmited by it have the opportunity to being received before it become stale.
pred  ConnectorMPS.staleProof {
	all t:Tick - tick/last,  c:Component, mp:MsgPassing
			| mp in this.buffer.t implies ((mp in this.buffer.(t.next) and mp.payload.freshness.end.lt[t.next]) or (mp.receiver).receive[c,mp.payload,t.next])
}

assert MPSCanNotBeStale  {
    (all c:ConnectorMPS | c.staleProof) implies ( all c1,c2:Component, d:Payload | no PayloadStaleness.holds[c1,c2,d])
}

check MPSCanNotBeStale for 3 but 5 Tick // No CounterExample must be found

/** Elevation of Privileges
* Refers to the ability of a component to gain capabilities without proper authorization to have such capabilities.
*/

sig SendReceiveElevation extends ComponentProperty {
}{
	not NoEop[comp, payl]
} 

// Verification that all messages are intercepted and injected with proper privileges.
pred NoEop[c:Component, d:Payload] {
   all t1:Tick, t2:Tick | let m = {m:MsgPassing | m.payload= d }{
          (inject[c,m,t1] implies Z_inject[c,m])
		  or (intercept[c,m,t2] implies Z_intercept[c,m])
    }
}

assert MPSCanBeEop  {
    all c:Component, d:Payload | no SendReceiveElevation.holds[c,d]
}

check MPSCanBeEop for 3 but 5 Tick // CounterExample must be found


// Security Req : A  EoPProof Component offers that these inject/intercept actions follow the correct Authorization 


pred Component.EoPProof {
    all m:MsgPassing, t:Tick | inject[this,m,t] implies Z_inject[this,m]
	all m:MsgPassing, t:Tick | intercept[this,m,t] implies Z_intercept[this,m]
}
	

assert MPSCanNotBeEop  {
    ( all c:Component | c.EoPProof) implies (all c:Component, d:Payload | no SendReceiveElevation.holds[c,d])
}

check MPSCanNotBeEop for 3 but 5 Tick // No CounterExample must be found


